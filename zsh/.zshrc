# Homebrew
eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

if type brew &>/dev/null
then
  FPATH="$(brew --prefix)/share/zsh/site-functions:${FPATH}"

  autoload -Uz compinit
  compinit
fi

source $DOTFILES/zsh/scripts.sh

# if tmux is executable, X is running, and not inside a tmux session, then try to attach.
# if attachment fails, start a new session
if [ -x "$(command -v tmux)" ] && [ -n "${DISPLAY}" ]; then
  [ -z "${TMUX}" ] && { tmux attach || tmux ; } >/dev/null 2>&1
  # [ -z "${TMUX}" ] && ftmuxp
fi

fpath=($ZDOTDIR/external $fpath)

autoload -Uz compinit; compinit
_comp_options+=(globdots) # With hidden files
source ~/.dotfiles/zsh/external/completion.zsh
source ~/.dotfiles/zsh/external/autosuggestions.zsh

autoload -Uz prompt_purification_setup; prompt_purification_setup

# Push the current directory visited on to the stack.
setopt AUTO_PUSHD
# Do not store duplicate directories in the stack
setopt PUSHD_IGNORE_DUPS
# Do not print the directory stack after using
setopt PUSHD_SILENT

bindkey -v
export KEYTIMEOUT=1

autoload -Uz cursor_mode && cursor_mode

zmodload zsh/complist
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history

# Clearing the shell is now done with CTRL+g
bindkey -r '^l'
bindkey -r '^g'
bindkey -s '^g' 'clear\n'

autoload -Uz edit-command-line
zle -N edit-command-line
bindkey -M vicmd v edit-command-line

source ~/.dotfiles/zsh/external/bd.zsh

if [ $(command -v "fzf") ]; then
    case "$OSTYPE" in
      linux*)
        # source /usr/share/fzf/completion.zsh
        # source /usr/share/fzf/key-bindings.zsh
        # source /usr/share/doc/fzf/examples/completion.zsh
        # source /usr/share/doc/fzf/examples/key-bindings.zsh
	source $HOME/.config/fzf/fzf.zsh
      ;;
      darwin*)
        source /usr/local/opt/fzf/shell/completion.zsh
        source /usr/local/opt/fzf/shell/key-bindings.zsh
      ;;
    esac
fi

if [ $(command -v "kubectl") ]; then
  source <(kubectl completion zsh)
  alias k="kubectl"
fi


case "$OSTYPE" in
  linux*)
    # Arch
    # source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
    # Ubuntu
    source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
  ;;
  darwin*)
    source /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
  ;;
esac

eval "$(jump shell)"
source "$XDG_CONFIG_HOME/zsh/aliases"
[[ "$OSTYPE" == darwin* ]] && source "$XDG_CONFIG_HOME/osx_aliases"

# Pyenv
export PYENV_ROOT="$HOME/.config/pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init --path)"

# NVM NodeJS
export NVM_DIR="$HOME/.config/nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# SDKMAN
#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$XDG_CONFIG_HOME/sdkman"
[[ -s "$SDKMAN_DIR/bin/sdkman-init.sh" ]] && source "$SDKMAN_DIR/bin/sdkman-init.sh"
