#!/bin/bash

CONFIGD="$HOME/.config"
DOTFILESD="$HOME/.dotfiles"
LOCALBIND="$HOME/.local/bin"


###########
# SCRIPTS #
###########
[ ! -d "$LOCALBIND" ] && mkdir -p $LOCALBIND
cp -r ./bin/* $LOCALBIND/

#######
# Zsh #
#######

rm -rf "$CONFIGD/zsh"
rm -rf "$HOME/.zshenv"

mkdir -p "$CONFIGD/zsh"
ln -sf "$DOTFILESD/zsh/.zshenv" "$HOME"
ln -sf "$DOTFILESD/zsh/.zshrc" "$CONFIGD/zsh"
ln -sf "$DOTFILESD/zsh/aliases" "$CONFIGD/zsh/aliases"
rm -rf "$CONFIGD/zsh/external"
ln -sf "$DOTFILESD/zsh/external" "$CONFIGD/zsh"

#######
# FZF #
#######

rm -rf "$CONFIGD/fzf"
mkdir -p "$CONFIGD/fzf"
ln -sf "$DOTFILESD/fzf/fzf.bash" "$CONFIGD/fzf"
ln -sf "$DOTFILESD/fzf/fzf.zsh" "$CONFIGD/fzf"


########
# WGET #
########

rm -rf "$CONFIGD/wget"
mkdir -p "$CONFIGD/wget"
ln -sf "$DOTFILESD/wget/wgetrc" "$CONFIGD/wget"
